package com.red.addressbook.mock;

import java.util.ArrayList;

import com.red.addressbook.helper.ScannerHelper;

public class ScannerHelperMock extends ScannerHelper {

	int index = 0;
	ArrayList<String> mockAnswers;
	
	public ScannerHelperMock() {
	}

	public ScannerHelperMock(ArrayList<String> mockAnswers) {
		setMockAnswers(mockAnswers);
	}

	@Override
	public String readInput() {
		String message = mockAnswers.get(index++);
		System.out.print(message);
		return message;
	}
	
	public void setMockAnswers(ArrayList<String> mockAnswers) {
		this.mockAnswers = mockAnswers;
		System.out.println();
		index = 0;
	}
}

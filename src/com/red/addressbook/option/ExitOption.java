package com.red.addressbook.option;

public class ExitOption implements Option {

	@Override
	public String getOptionName() {
		return "Exit";
	}
	
	@Override
	public boolean run() {
		System.out.println("\nBye Bye! Thank you!");
		System.exit(0);
		return false;
	}


}

package com.red.addressbook.mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.red.addressbook.database.Repository;
import com.red.addressbook.entity.Contact;

public class ContactRepositoryMock implements Repository<Contact> {

	public ArrayList<Contact> mockContacts = new ArrayList<>();
	public ContactRepositoryMock() {
		mockContacts.add(new Contact(1, "Frodo", "Baggins", "(21) 222-3333", "The Shire. 24"));
		mockContacts.add(new Contact(2, "Super", "Man", "(11) 667-2222", "Justice League"));
		mockContacts.add(new Contact(3, "Batman", "Wayne", "(11) 325-7844", "Batcave, on Brunce Wayne basement"));
		mockContacts.add(new Contact(4, "Charles", "Xavier", "(21) 123-455", "School for powerful and awesome people"));
		mockContacts.add(new Contact(5, "Sheldon", "Copper", "(13) 345-7777", "His spot"));
	}

	@Override
	public String createTable() {
		return "";
	}

	@Override
	public void add(Contact entity) {
		mockContacts.add(entity);
	}

	@Override
	public Collection<Contact> getAll(String orderBy) {
		return mockContacts;
	}

	@Override
	public boolean update(Contact entity) {
		Contact contact = 
				mockContacts.stream().filter(c -> c.getId() == entity.getId()).findFirst().get();
		
		if(contact == null)
			return false;
		
		contact.setName(entity.getName())
			.setLastName(entity.getLastName())
			.setPhone(entity.getPhone())
			.setAddress(entity.getAddress());
		
		return true;
	}

	@Override
	public boolean remove(Contact entity) {
		for (Iterator<Contact> i = mockContacts.iterator(); i.hasNext();) {
			Contact contact = i.next();
		    if (contact.getId() == entity.getId()) {
		        i.remove();
		        return true;
		    }
		}
		return false;
	}

	@Override
	public Contact getById(Integer id) {
		return mockContacts.stream().filter(c -> c.getId() == id).findFirst().get();
	}

}

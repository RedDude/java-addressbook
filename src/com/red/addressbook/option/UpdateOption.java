package com.red.addressbook.option;

import com.red.addressbook.entity.Contact;
import com.red.addressbook.service.ContactService;

public class UpdateOption implements Option {

	private String message = "Update a Contact (hint: type 'cancel' to quit)";

	@Override
	public String getOptionName() {
		return  "Update";
	}

	@Override
	public boolean run() {
		show();
		return false;
	}

	public void show() {
		System.out.println(message);
		ContactService service = new ContactService();
		Integer id = service.readId();
		if (id != null) {
			Contact contact = service.getById(id);
			if (contact == null) {
				System.out.println("Contact not found.\n");
				return;
			}
			service.readAndUpdate(contact);
		}
	}
	
}

package com.red.addressbook.view;

import java.util.Arrays;
import java.util.List;

import com.red.addressbook.option.BackOption;
import com.red.addressbook.option.ListByLastName;
import com.red.addressbook.option.ListByNameOption;
import com.red.addressbook.option.Option;

public class ListView extends BaseView {

	private List<Option> options = Arrays.asList(new ListByNameOption(), new ListByLastName(), new BackOption());
	String message = "List by:";
	public void show() {
		System.out.println(message);

		boolean back = false;
		while (!back) {
			back = showOptions(options);
		}
		
	}

}

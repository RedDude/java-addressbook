package com.red.addressbook.option;

import com.red.addressbook.entity.Contact;
import com.red.addressbook.service.ContactService;

public class DetailsOption implements Option {

	private String message = "Show Detail of a Contact (hint: type 'cancel' to quit)";

	@Override
	public String getOptionName() {
		return "Show Contact Details";
	}

	@Override
	public boolean run() {
		show();
		return false;
	}

	public void show() {
		System.out.println(message);
		ContactService service = new ContactService();
		Integer id = service.readId();
		if (id != null) {
			Contact contact = service.getById(id);
			if (contact == null) {
				System.out.println("Contact not found.\n");
				return;
			}
			System.out.println("\n");
			System.out.println(contact.infoCard(false));
		}
	}

}

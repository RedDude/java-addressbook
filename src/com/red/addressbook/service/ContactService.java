package com.red.addressbook.service;

import java.util.Collection;
import java.util.HashMap;

import com.red.addressbook.database.ContactRepository;
import com.red.addressbook.database.Repository;
import com.red.addressbook.entity.Contact;
import com.red.addressbook.entity.Contact.Fields;
import com.red.addressbook.helper.ScannerHelper;

public class ContactService {

	Repository<Contact> repository;
	ScannerHelper scanner;
	
	public ContactService() {
		 repository = new ContactRepository();
		 scanner = new ScannerHelper();
	}
	
	public ContactService(Repository<Contact> repository) {
		super();
		this.repository = repository;
		scanner = new ScannerHelper();
	}

	public ContactService(Repository<Contact> repository, ScannerHelper scanner) {
		super();
		this.repository = repository;
		this.scanner = scanner;
	}
	
	public Contact readContact() {
		return readContact(null);
	}

	public Contact readContact(Contact entity) {
		HashMap<Fields, String> values = new HashMap<Fields, String>();
		boolean isEdit = entity != null;

		for (Fields field : Contact.Fields.values()) {
			String result;

			if (isEdit) {
				result = scanner.readValue(field.getName(), field.getValue(entity));
			} else {
				result = scanner.readValue(field.getName());
			}

			if (ScannerHelper.hasCanceled(result)) {
				return null;
			}
			values.put(field, result);
		}

		return new Contact(values.get(Fields.NAME), 
				values.get(Fields.LAST_NAME), 
				values.get(Fields.PHONE),
				values.get(Fields.ADDRESS));
	}

	public Integer readId() {
		String result = scanner.readValue("id", ScannerHelper.CANCEL);

		if (ScannerHelper.hasCanceled(result)) {
			return null;
		}

		return scanner.findNumber(result);
	}

	public void readAndAdd() {
		add(readContact());
	}

	public void readAndUpdate(Contact contact) {
		if(contact != null && contact.getId() != null) {
			Contact updateContact = readContact(contact);
			if(updateContact != null) {
				updateContact.setId(contact.getId());
			}
			update(updateContact);
		}
	}

	public void add(Contact entity) {
		if (entity != null)
			repository.add(entity);
	}

	public Collection<Contact> getAll(String orderBy) {
		return repository.getAll(orderBy);
	}

	public Contact getById(Integer id) {
		return repository.getById(id);
	}

	public boolean delete(Integer id) {
		if (id != null)
			return repository.remove(new Contact(id));
		return false;
	}

	public void update(Contact entity) {
		repository.update(entity);
	}

	public ScannerHelper getScanner() {
		return scanner;
	}

	public void setScanner(ScannerHelper scanner) {
		this.scanner = scanner;
	}

}

package com.red.addressbook.view;

import com.red.addressbook.entity.Contact;
import com.red.addressbook.service.ContactService;

public class MainRunner {

	public static void main(String args[]) {
		bootstrap();
		new MainView(args);
	}

	static void bootstrap() {
		ContactService service = new ContactService();
		service.add(new Contact(1, "Frodo", "Baggins", "(21) 222-3333", "The Shire. 24"));
		service.add(new Contact(2, "Super", "Man", "(11) 667-2222", "Justice League"));
		service.add(new Contact(3, "Batman", "Wayne", "(11) 325-7844", "Batcave, on Brunce Wayne basement"));
		service.add(new Contact(4, "Charles", "Xavier", "(21) 123-455", "School for powerful and awesome people"));
		service.add(new Contact(5, "Sheldon", "Copper", "(13) 345-7777", "His spot"));
	}
}

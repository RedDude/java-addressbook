package com.red.addressbook.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Store {
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String CONNECTION_NAME = "jdbc:sqlite:addressbook.sqlite";
	
	private static final Store INSTANCE = new Store();

	private Store() {
	}

	//	Usually I would avoid to use a Singleton, they sometimes can become an anti-pattern.
	//	but to a small app like this, a singleton can be pretty usefully.
	//	in a real application with multithread, I also would write it in a way to avoid race conditions
	public static Store getInstance() {
		return INSTANCE;
	}

	public Connection getConnection() {
		Connection c = null;
		try {
			Class.forName(DRIVER);
			c = DriverManager.getConnection(CONNECTION_NAME);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return c;
	}

	public void createDB(Repository<?> repository) {
		Connection c = null;
		Statement stmt = null;
		try {
			c = getConnection();

			stmt = c.createStatement();
			String sql = repository.createTable();
			stmt.executeUpdate(sql);
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}
	
}
package com.red.addressbook.database;

import java.util.Collection;

public interface Repository<T> {

	public String createTable();
    public void add(T entity);
    public Collection<T> getAll(String orderBy);
    public T getById(Integer id);
    public boolean remove(T entity);
    public boolean update(T entity);

}

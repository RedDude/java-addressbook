package com.red.addressbook.option;

import com.red.addressbook.service.ContactService;

public class DeleteOption implements Option {

	private String message = "Delete Contact (hint: type 'cancel' to quit)";
	
	@Override
	public String getOptionName() {
		return "Delete";
	}
	
	@Override
	public boolean run() {
		show();
		return false;
	}

	public void show() {
		System.out.println(message);
		ContactService service = new ContactService();
		Integer id = service.readId();
		if (id != null) {
			service.delete(id);
		}
	}


}

package com.red.addressbook.helper;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.red.addressbook.option.Option;

public class ScannerHelper {

	// Could be in a config file, but KISS
	private static final int MAX_LENGTH = 255;
	public static final String CANCEL = "cancel";
	Scanner scanner;
	
	public ScannerHelper() {

	}

	public Option readOption(List<Option> options) {
		String option = readValue("option");
		Option selectedOption = null;

		Integer number = findNumber(option);
		if (number != null) {
			selectedOption = options.get(number - 1);
		}

		if (selectedOption == null) {
			for (int i = 0; i < options.size(); i++) {
				String avaliableOption = options.get(i).getOptionName().toLowerCase();
				if (option.equals(avaliableOption)) {
					selectedOption = options.get(i);
					break;
				}
			}
		}

		return selectedOption;
	}

	public static boolean hasCanceled(String result) {
		return result.equals(CANCEL);
	}

	public Integer findNumber(String value) {
		Pattern p = Pattern.compile(".*?(?<number>\\d+).*?");
		Matcher m = p.matcher(value);

		while (m.find()) {
			String number = m.group("number");
			return new Integer(number);
		}

		return null;
	}

	public String readValue(String field) {
		System.out.println(field + ": ");
		String value = readInput();

		if (value.length() > MAX_LENGTH) {
			value.substring(0, MAX_LENGTH);
		}

		return value;
	}

	public String readValue(String field, String defaultValue) {
		String value = readValue(field + " (default: " + defaultValue + ")");
		if (value.isEmpty()) {
			value = defaultValue;
		}
		return value;
	}

	public String readInput() {
		scanner = new Scanner(System.in);
		String value = scanner.nextLine().trim().toLowerCase();
		return value;
	}
}

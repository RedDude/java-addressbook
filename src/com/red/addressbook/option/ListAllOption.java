package com.red.addressbook.option;

import com.red.addressbook.view.ListView;

public class ListAllOption implements Option {

	@Override
	public String getOptionName() {
		return "List All";
	}
	
	@Override
	public boolean run() {
		new ListView().show();
		return false;
	}


}

package com.red.addressbook.option;

import java.util.ArrayList;

import com.red.addressbook.entity.Contact;
import com.red.addressbook.service.ContactService;

public class ListByLastName implements Option {

	@Override
	public String getOptionName() {
		return "Last Name";
	}
	
	@Override
	public boolean run() {
		ContactService service = new ContactService();
		ArrayList<Contact> contacts = (ArrayList<Contact>) service.getAll("last_name");

		StringBuilder sb = new StringBuilder();

		for (Contact contact : contacts) {
			sb.append(contact.infoCard(false));
			sb.append("\n+------------------------+\n\n");
		}
		
		System.out.println(sb);
		return true;
	}


}

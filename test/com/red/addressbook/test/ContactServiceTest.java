package com.red.addressbook.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.red.addressbook.entity.Contact;
import com.red.addressbook.mock.ContactRepositoryMock;
import com.red.addressbook.mock.ScannerHelperMock;
import com.red.addressbook.service.ContactService;

class ContactServiceTest {

	private ContactService service;
	private Contact contactTest;
	private ContactRepositoryMock mock;
	private ScannerHelperMock scanner;
	
	@BeforeEach
	void setUp() throws Exception {
		// To avoid this many instantiations a Dependency Injector would do the trick
		// Instead of creating many mocks classes, it's possible to use a lib like Mockito would help
		mock = new ContactRepositoryMock();
		scanner = new ScannerHelperMock();
		contactTest = mock.mockContacts.get(0);
		service = new ContactService(mock, scanner);
	}

	@Test
	void retriveAll() {
		Collection<Contact> all = service.getAll("name");
		assertNotNull(all);
		assertEquals(all, mock.mockContacts);
	}
	
	@Test
	void add() {
		service.add(contactTest);
		Contact justAddedContact = service.getById(contactTest.getId());
		assertNotNull(justAddedContact);
		assertEquals(contactTest, justAddedContact);
	}
	
	@Test
	void getById() {
		Contact getByIdContact = service.getById(contactTest.getId());
		assertNotNull(getByIdContact);
		assertEquals(contactTest, getByIdContact);
	}
	
	@Test
	void update() {
		contactTest.setName("Sam");
		contactTest.setLastName("Gamgee");
		
		service.update(contactTest);
		Contact getByIdUpdateContact = service.getById(contactTest.getId());
		assertNotNull(getByIdUpdateContact);
		assertEquals(contactTest, getByIdUpdateContact);
	}
	
	@Test
	void readContact() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add(contactTest.getName());
		mockAnswers.add(contactTest.getLastName());
		mockAnswers.add(contactTest.getPhone());
		mockAnswers.add(contactTest.getAddress());
		scanner.setMockAnswers(mockAnswers);
		
		Contact contact = service.readContact();
		assertEquals(contactTest, contact);
	}
	
	@Test
	void readContactWithDefaults() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add("Sam");
		mockAnswers.add("Gamgee");
		mockAnswers.add("");
		mockAnswers.add("");
		scanner.setMockAnswers(mockAnswers);
		
		Contact contact = service.readContact(contactTest);
		assertEquals(contactTest.getPhone(), contact.getPhone());
		assertEquals(contactTest.getAddress(), contact.getAddress());
	}
	

	@Test
	void canCancel() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add("Sam");
		mockAnswers.add("Gamgee");
		mockAnswers.add("cancel");
		scanner.setMockAnswers(mockAnswers);
		
		Contact contact = service.readContact(contactTest);
		assertNull(contact);
	}
	
	
	@Test
	void readId() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add("15");
		scanner.setMockAnswers(mockAnswers);
		
		Integer id = service.readId();
		assertTrue(id == 15);
	}
	
	@Test
	void readIdWithSomeJibberish() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add("jibberish15jibberish");
		scanner.setMockAnswers(mockAnswers);
		
		Integer id = service.readId();
		assertTrue(id == 15);
	}
	
	@Test
	void noReadId() {
		ArrayList<String> mockAnswers = new ArrayList<String>();
		
		mockAnswers.add("jibberish");
		scanner.setMockAnswers(mockAnswers);
		
		Integer id = service.readId();
		assertNull(id);
	}

}

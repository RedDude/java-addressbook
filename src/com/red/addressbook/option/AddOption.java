package com.red.addressbook.option;

import com.red.addressbook.service.ContactService;

public class AddOption implements Option {

	private String message = "To create a new Contact. \nPlease type: (hint: type cancel to quit)";

	
	@Override
	public String getOptionName() {
		return "Add";
	}
	
	@Override
	public boolean run() {
		show();
		return false;
	}
	
	public void show() {
		System.out.println(message);
		ContactService service = new ContactService();
		service.readAndAdd();
	}


}

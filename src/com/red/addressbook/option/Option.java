package com.red.addressbook.option;

public interface Option {

	String getOptionName();
	boolean run();
}

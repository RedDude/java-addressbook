package com.red.addressbook.entity;

import java.util.function.Function;

public class Contact implements Comparable<Contact> {
	private Integer id;
	private String name;
	private String lastName;
	private String phone;
	private String address;

    // Could use annotations or reflection, but KISS (well almost)
	public enum Fields {
		NAME("First Name", Contact::getName),
		LAST_NAME("Last Name", Contact::getLastName),
		PHONE("Phone", Contact::getPhone),
		ADDRESS("Address", Contact::getAddress);

		private final String name;
		private final Function<Contact, String> get;
		private Fields(final String name, Function<Contact, String> get) {
		    this.name = name;
		    this.get = get;
		}

		public String getName() {
		    return name;
		}
		
		public String getValue(Contact contact) {
		    return get.apply(contact);
		}
		
    };
    
	public Contact(Integer id) {
		this.id = id;
	}

	public Contact(String name, String lastName, String phone, String address) {
		setName(name);
		setLastName(lastName);
		setPhone(phone);
		setAddress(address);

		if (this.name.isEmpty() && this.phone.isEmpty()) {
			throw new IllegalStateException("Either the name or phone are required");
		}
	}

	public Contact(Integer id, String name, String lastName, String phone, String address) {
		this(name, lastName, phone, address);
		setId(id);
	}

	private String setFieldOrDefault(String field) {
		if (field != null) {
			return field.trim();
		}
		return "";
	}

	public boolean equals(Object other) {
		if (other instanceof Contact) {
			Contact otherContact = (Contact) other;
			return name.equals(otherContact.getName()) && lastName.equals(otherContact.getLastName())
					&& phone.equals(otherContact.getPhone()) && address.equals(otherContact.getAddress());
		}
		return false;
	}

	public String infoCard(Boolean lastNameFirst) {
		String displayName = lastNameFirst ? name + " " + lastName : lastName + ", " + name;
		return id + " | " + displayName + "\nPhone: " + phone + "\nAddress: " + address;
	}

	public String toString() {
		return id + " | " + name + " " + lastName + "\n" + phone + "\n" + address;
	}

	public int compareTo(Contact otherContact) {
		int comparison = name.compareTo(otherContact.getName());
		if (comparison != 0) {
			return comparison;
		}
		comparison = phone.compareTo(otherContact.getPhone());
		if (comparison != 0) {
			return comparison;
		}
		return address.compareTo(otherContact.getAddress());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer identifier) {
		this.id = identifier;
	}
	
	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public Contact setName(String name) {
		this.name = setFieldOrDefault(name);
		return this;
	}

	public Contact setLastName(String lastName) {
		this.lastName = setFieldOrDefault(lastName);
		return this;
	}

	public Contact setPhone(String phone) {
		this.phone = setFieldOrDefault(phone);
		return this;
	}

	public Contact setAddress(String address) {
		this.address = setFieldOrDefault(address);
		return this;
	}

}
package com.red.addressbook.view;

import java.util.Arrays;
import java.util.List;

import com.red.addressbook.option.AddOption;
import com.red.addressbook.option.DeleteOption;
import com.red.addressbook.option.DetailsOption;
import com.red.addressbook.option.ExitOption;
import com.red.addressbook.option.ListAllOption;
import com.red.addressbook.option.Option;
import com.red.addressbook.option.UpdateOption;

public class MainView extends BaseView {

	private String message = "Welcome to your Addressbook.";
	private List<Option> options = Arrays.asList(new ListAllOption(), new DetailsOption(), new AddOption(),
			new UpdateOption(), new DeleteOption(), new ExitOption());

	MainView(String args[]) {
		System.out.println(message);

		while (true) {
			showOptions(options);
		}
	}

}

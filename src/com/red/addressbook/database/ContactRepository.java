package com.red.addressbook.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

import com.red.addressbook.entity.Contact;

public class ContactRepository implements Repository<Contact> {

	public ContactRepository() {
		Store.getInstance().createDB(this);
	}

	@Override
	public String createTable() {
		return "CREATE TABLE IF NOT EXISTS contact (ID INTEGER PRIMARY KEY autoincrement, name CHAR(255), "
				+ " last_name CHAR(255), phone CHAR(255), address CHAR(255), date_added datetime)";
	}

	@Override
	public void add(Contact entity) {
		Connection c = null;
		try {
			c = Store.getInstance().getConnection();

			String sql = "INSERT INTO contact (name, last_name, phone, address, date_added, id)"
					+ " values (?, ?, ?, ?, ?, ?)";

			PreparedStatement preparedStmt = c.prepareStatement(sql);
			preparedStmt.setString(1, entity.getName());
			preparedStmt.setString(2, entity.getLastName());
			preparedStmt.setString(3, entity.getPhone());
			preparedStmt.setString(4, entity.getAddress());
			preparedStmt.setDate(5, new Date(System.currentTimeMillis()));
			preparedStmt.setObject(6, entity.getId());

			preparedStmt.execute();
			c.close();
		} catch (Exception e) {
			// just to avoid errors at startup
			if (!e.getMessage().contains("[SQLITE_CONSTRAINT_PRIMARYKEY]")) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
			}
		}
	}

	@Override
	public Collection<Contact> getAll(String orderBy) {
		Connection c = null;
		Statement stmt = null;
		ArrayList<Contact> contacts = new ArrayList<Contact>();

		try {
			c = Store.getInstance().getConnection();

			if (orderBy == null) {
				orderBy = "id";
			}
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM contact ORDER BY " + orderBy + " COLLATE NOCASE");

			while (rs.next()) {
				// an auto mapper will be pretty usefully, but lets KISS
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String lastName = rs.getString("last_name");
				String phone = rs.getString("phone");
				String address = rs.getString("address");

				Contact contact = new Contact(name, lastName, phone, address);
				contact.setId(id);
				contacts.add(contact);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return contacts;
	}

	@Override
	public boolean update(Contact entity) {
		Connection c = null;
		try {
			c = Store.getInstance().getConnection();

			String sql = "UPDATE contact set name = ?, last_name = ?, phone = ?, address = ?, "
					+ "date_added = ? WHERE id = ?";

			PreparedStatement preparedStmt = c.prepareStatement(sql);
			preparedStmt.setString(1, entity.getName());
			preparedStmt.setString(2, entity.getLastName());
			preparedStmt.setString(3, entity.getName());
			preparedStmt.setString(4, entity.getAddress());
			preparedStmt.setDate(5, new Date(System.currentTimeMillis()));
			preparedStmt.setInt(6, entity.getId());

			boolean result = preparedStmt.execute();
			c.close();
			return result;
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return false;
	}

	@Override
	public boolean remove(Contact entity) {
		Connection c = null;
		try {
			c = Store.getInstance().getConnection();

			String sql = "DELETE FROM contact WHERE ID = ?;";
			PreparedStatement preparedStmt = c.prepareStatement(sql);
			preparedStmt.setInt(1, entity.getId());

			boolean result = preparedStmt.execute();
			c.close();
			return result;
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return false;
	}

	@Override
	public Contact getById(Integer id) {
		Contact contact = null;
		Connection c = null;
		try {
			c = Store.getInstance().getConnection();

			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM contact WHERE ID = " + id);
			while (rs.next()) {
				// an auto mapper would be pretty usefully, but lets KISS
				String name = rs.getString("name");
				String lastName = rs.getString("last_name");
				String phone = rs.getString("phone");
				String address = rs.getString("address");

				contact = new Contact(id, name, lastName, phone, address);
			}
			rs.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		return contact;
	}

}

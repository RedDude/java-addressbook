package com.red.addressbook.view;

import java.util.List;

import com.red.addressbook.helper.ScannerHelper;
import com.red.addressbook.option.Option;

public class BaseView {

	public boolean showOptions(List<Option> options) {
		System.out.println(" \nPlease type an option:");
		for (int i = 0; i < options.size(); i++) {
			System.out.println("\t" + (i + 1) + " - " + options.get(i).getOptionName());
		}
		ScannerHelper scanner = new ScannerHelper();
		
		try {
			Option option = scanner.readOption(options);
			if (option != null) {
				System.out.println("\n");
				return option.run();
			}
			System.out.println("\n\nInvalid Option, please select an avaliable option.\n");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("\n\nSomething pretty bad just happend! Lets start again.\n");
		}
		return false;
	}
}
